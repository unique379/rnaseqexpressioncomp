# RNASeqExpressionComp v1.0

The script will compare the two given matrix (dataframe) with gene and expressions obtained from two methods/pipeline.

How to run:

1. get help

rnaseqexpressioncomp/RNAseqCommandLineBenchmarking.R -h

2. Run with example or use your own expression matrix to compare

rnaseqexpressioncomp/RNAseqCommandLineBenchmarking.R rnaseqexpressioncomp/testMatrix/ENCFF419GVS.genesCivet.txt rnaseqexpressioncomp/testMatrix/ENCFF419GVS.genesNF.txt civet nextflow civet_vs_nexflow

#### Note: The tool assumes R is installed and set in your global environment. Required packages will be installed in case of missing, assuming you have writable permission in your system.

Output: Example output will be a plot along with filtered expression (>0). Please take a look in rnaseqexpressioncomp/outputs


Please contact in case of any bugs/feedback:

Dr. Rupesh Kesharwani

bioinforupesh2009.au@gmail.com
